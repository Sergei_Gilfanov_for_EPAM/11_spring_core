package org.shop;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.shop.api.ProductService;
import org.shop.api.ProposalService;
import org.shop.api.SellerService;
import org.shop.api.ServiceConfig;
import org.shop.api.UserService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

@Configuration
@Import(ServiceConfig.class)
public class ShopLauncher {
  private static final Log log = LogFactory.getLog(ShopLauncher.class);

  @Bean
  public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
    PropertySourcesPlaceholderConfigurer retval = new PropertySourcesPlaceholderConfigurer();
    retval.setLocation(new ClassPathResource("/org/shop/shop.properties"));
    return retval;
  }

  @Bean
  public ProductInitializer productInitializer(ProductService productService) {
    return new ProductInitializer(productService);
  }

  @Bean
  public ProposalInitializer proposalInitializerWithStrangeName(ProductService productService,
      ProposalService proposalService, SellerService sellerService) {
    return new ProposalInitializer(productService, proposalService, sellerService);
  }

  @Bean
  public SellerInitializer sellerInitializer(SellerService sellerService) {
    Map<Long, String> sellerNames = new HashMap<Long, String>();
    sellerNames.put(1L, "Amazon");
    sellerNames.put(2L, "Samsung");
    sellerNames.put(3L, "Seller 3");

    return new SellerInitializer(sellerService, sellerNames);
  }

  @Bean
  public UserInitializer userInitializer(UserService userService) {
    return new UserInitializer(userService);
  }

  @Bean(initMethod = "initData")
  public DataInitializer dataInitializer(SellerInitializer sellerInitializer,
      ProductInitializer productInitializer, UserInitializer userInitializer) {
    return new DataInitializer(sellerInitializer, productInitializer, userInitializer);
  }

  /**
   * The main method.
   *
   * @param args the arguments
   * @throws InterruptedException
   */
  public static void main(String[] args) {
    AnnotationConfigApplicationContext ctx =
        new AnnotationConfigApplicationContext(ShopLauncher.class);
    ctx.close();
  }
}
