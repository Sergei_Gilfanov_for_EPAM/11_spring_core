package org.shop.repository.factory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FactoryConfig {
    @Bean
    public UserRepositoryFactory userRepositoryFactory() {
      return new UserRepositoryFactory();
    }
}
