package org.shop.repository;

import org.shop.repository.factory.FactoryConfig;
import org.shop.repository.factory.UserRepositoryFactory;
import org.shop.repository.map.ItemMapRepository;
import org.shop.repository.map.OrderMapRepository;
import org.shop.repository.map.ProductMapRepository;
import org.shop.repository.map.ProposalMapRepository;
import org.shop.repository.map.SellerMapRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(FactoryConfig.class)
public class RepositoryConfig {

  @Bean
  public ItemRepository itemRepository() {
    return new ItemMapRepository();
  }

  @Bean
  public OrderRepository orderRepository(
      @Value("${orderRepository.initialSequence}") long initialSequence) {
    OrderMapRepository retval = new OrderMapRepository();
    retval.setSequence(initialSequence);
    return retval;
  }

  @Bean
  public ProductRepository productRepository() {
    return new ProductMapRepository();
  }

  @Bean
  public ProposalRepository proposalRepository() {
    return new ProposalMapRepository();
  }

  @Bean
  public SellerRepository sellerRepository() {
    return new SellerMapRepository();
  }

  @Bean
  public UserRepository userRepository(UserRepositoryFactory userRepositoryFactory) {
    return userRepositoryFactory.createUserRepository();
  }
}
