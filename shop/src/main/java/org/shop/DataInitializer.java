package org.shop;

import javax.annotation.Resource;

/**
 * The main Data Initializer util class.
 */
public class DataInitializer {

    /** The seller initializer. */
    private SellerInitializer sellerInitializer;
    
    /** The product initializer. */
    private ProductInitializer productInitializer;
    
    /** The proposal initializer. */
    @Resource(name="proposalInitializerWithStrangeName")
    private ProposalInitializer proposalInitializer;
    
    /** The user initializer. */
    private UserInitializer userInitializer;

    public DataInitializer(SellerInitializer sellerInitializer, ProductInitializer productInitializer, UserInitializer userInitializer) {
      super();
      this.sellerInitializer = sellerInitializer;
      this.productInitializer = productInitializer;
      this.userInitializer = userInitializer;
    }
    /**
     * Inits the data.
     */
    public void initData() {
        sellerInitializer.initSellers();
        userInitializer.initUsers();
        productInitializer.initProducts();
        proposalInitializer.initProposals();
    }
}
