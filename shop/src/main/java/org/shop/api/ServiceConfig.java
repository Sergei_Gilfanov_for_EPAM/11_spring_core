package org.shop.api;

import org.shop.api.impl.ItemServiceImpl;
import org.shop.api.impl.OrderServiceImpl;
import org.shop.api.impl.ProductServiceImpl;
import org.shop.api.impl.ProposalServiceImpl;
import org.shop.api.impl.SellerServiceImpl;
import org.shop.api.impl.UserServiceImpl;
import org.shop.repository.ItemRepository;
import org.shop.repository.OrderRepository;
import org.shop.repository.ProductRepository;
import org.shop.repository.ProposalRepository;
import org.shop.repository.RepositoryConfig;
import org.shop.repository.SellerRepository;
import org.shop.repository.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(RepositoryConfig.class)
public class ServiceConfig {
    @Bean
    public ItemService itemService(ItemRepository repository) {
      return new ItemServiceImpl(repository);
    }

    @Bean
    public OrderService orderService(OrderRepository orderRepository, ItemService itemService) {
      return new OrderServiceImpl(orderRepository, itemService);
    }

    @Bean
    public ProductService productService(ProductRepository repository) {
      return new ProductServiceImpl(repository);
    }

  @Bean
  public ProposalService proposalService(ProposalRepository repository, SellerService sellerService,
      ProductService productService) {
    return new ProposalServiceImpl(repository, sellerService, productService);
  }

    @Bean
    public SellerService sellerService(SellerRepository repository) {
      return new SellerServiceImpl(repository);
    }

    @Bean
    public UserService userService(UserRepository repository) {
      return new UserServiceImpl(repository);
    }

}
