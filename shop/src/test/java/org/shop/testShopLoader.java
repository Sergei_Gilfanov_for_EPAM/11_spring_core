package org.shop;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.shop.api.ProductService;
import org.shop.api.ProposalService;
import org.shop.api.SellerService;
import org.shop.api.UserService;
import org.shop.data.Seller;
import org.shop.data.User;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class testShopLoader {
  static AnnotationConfigApplicationContext ctx;

  @BeforeClass
  public static void setUp() throws Exception {
    ctx = new AnnotationConfigApplicationContext(ShopLauncher.class);
  }

  @AfterClass
  public static void tearDown() throws Exception {
    ctx.close();
  }

  @Test
  public void productDataShouldBeFilled() {
    ProductService productService = ctx.getBean(ProductService.class);
    List<String> actualProducts = productService.getProducts().stream().map((product) -> {
      return product.getId().toString() + '|' + product.getName();
    }).collect(Collectors.toList());

    List<String> expectedProducts = Arrays.asList(new String[] {"1|Samsung Galaxy Tab",
        "2|Kindle Fire", "3|Kindle Touch", "4|Samsung S5830L Galaxy Ace"});
    assertThat(actualProducts, is(expectedProducts));
  }

  @Test
  public void amazonProposalsShouldBeRight() {
    ProposalService proposalService = ctx.getBean(ProposalService.class);
    long sellerId = 1L; // Amazon
    List<String> actualProposals =
        proposalService.getProposalsBySellerId(sellerId).stream().map((proposal) -> {
          return proposal.getProduct().getName() + "|" + Double.toString(proposal.getPrice());
        }).collect(Collectors.toList());

    List<String> expectedProposals =
        Arrays.asList(new String[] {"Kindle Fire|199.0", "Kindle Touch|99.0"});
    assertThat(actualProposals, is(expectedProposals));
  }

  @Test
  public void samsungProposalsShouldBeRight() {
    ProposalService proposalService = ctx.getBean(ProposalService.class);
    long sellerId = 2L; // Samsung
    List<String> actualProposals =
        proposalService.getProposalsBySellerId(sellerId).stream().map((proposal) -> {
          return proposal.getProduct().getName() + "|" + Double.toString(proposal.getPrice());
        }).collect(Collectors.toList());

    List<String> expectedProposals =
        Arrays.asList(new String[] {"Samsung S5830L Galaxy Ace|250.0", "Samsung Galaxy Tab|500.0"});
    assertThat(actualProposals, is(expectedProposals));
  }

  @Test
  public void sellersTableShouldBeRight() {
    SellerService sellerService = ctx.getBean(SellerService.class);
    List<String> actualSellers =
        sellerService.getSellers().stream().map(Seller::toString).collect(Collectors.toList());
    List<String> expectedSellers = Arrays.asList(new String[] {"Seller [id=1, name=Amazon]",
        "Seller [id=2, name=Samsung]", "Seller [id=3, name=Seller 3]"});
    assertThat(actualSellers, is(expectedSellers));
  }

  @Test
  public void userTableShouldBeRight() {
    UserService userService = ctx.getBean(UserService.class);
    List<String> actualUsers =
        userService.getUsers().stream().map(User::toString).collect(Collectors.toList());

    List<String> expectedUsers = Arrays.asList(
        new String[] {"User [id=1, username=Ivan Ivanov]", "User [id=2, username=Petr Petrov]"});
    assertThat(actualUsers, is(expectedUsers));
  }
}
